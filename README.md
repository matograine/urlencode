This repo is a contribution to Solderpunk's OFFLIRSOCH.

It contains two Python scripts urlencode.py and urldecode.py, which, well, en/decode URL-formatted strings.

Arguments can be passed or piped to the scripts.

I added these aliases to my .bashrc for ease of use:
```bash
alias urlencode='~/scripts/urlencode/urlencode.py'
alias urldecode='~/scripts/urlencode/urldecode.py'
```

Feel free to do whatever you want with this code;