#!/bin/python
# This script decodes URLs.
# Feel free to do whatever you want with it.

import sys
from urllib.parse import quote_plus

if len(sys.argv) == 1:
    strinput = input()
else:
    strinput = sys.argv[1]

print(quote_plus(strinput))
