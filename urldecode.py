#!/bin/python
# This script decodes URLs.
# Feel free to do whatever you want with it.

import sys
from urllib.parse import unquote_plus

if len(sys.argv) == 1:
    strinput = input()
else:
    strinput = sys.argv[1]

print(unquote_plus(strinput))
